
# Neo-Copy
This is a tool for SAP Cloud Platform that will copy all Business rules or workflows from one tenant to another.
It will only copy  the workflows and Business rules where the change date on the source is greater then the change date on the target tenant.
## Build
We use gradle to build the tool.
Clone the repo and just run
```sh
gradle run
```
Compiled code result will be stored in build/distributions.

## Usage example

It is an executable jar file. 
You have to define following parameters:

### global parameters
| Parameter     | Description  |
| ------------- |-------------|
| h  | Host. default is eu.hana.ondemand.com target and source need to be on the same host (for now) |
| sa | Source account ID where to copy from |
| ta | Target tenant ID where to copy to |
| t | Type of object to copy. At the moment only BRM (Business rules) and WORKFLOW are available |
| f (optional) | Filter on object name. If a filter is set, the object is transported without checking the change date |

### workflow
For WORKFLOW you have to define 2 oauth clients with grant type client credential.
| Parameter     | Description  |
| ------------- |-------------|
| soc_wr  | Source oAuth client ID for workflow. Used to read/export the workflow objects |
| socs_wr | Secret for the oAuth client defined in soc_wr |
| toc_wr  | Target oAuth client ID for  workflow. Used to read & import the workflow objects |
| tocs_wr | Secret for the oAuth client defined in toc_wr |

```sh
// Copy workflows
java -jar neo-copy.jar -h eu3.hana.ondemand.com -t workflow -sa i459s1xnyc -ta hwzkkm3cm7 -soc_wr <wfruntime_oauth_client_source> -socs_wr <wfruntime_oauth_client_secret_source> -toc_wr <wfruntime_oauth_client_target> -tocs_wr <wfruntime_oauth_client_secret_target>
```

### Business rules (BRM)
For BRM you have to define 3 oauth clients with grant type client credential.

| Parameter     | Description  |
| ------------- |-------------|
| soc_rr  | Source oAuth client ID for rules repository. Used to read/export the rule objects |
| socs_rr | Secret for the oAuth client defined in soc_rr |
| toc_rr  | Target oAuth client ID for rules repository. Used to read & import the rule objects |
| tocs_rr | Secret for the oAuth client defined in toc_rr |
| toc_rruntime  | Target oAuth client ID for rules repository. Used to deploy the rule services |
| tocs_rruntime | Secret for the oAuth client defined in toc_rruntime |

```sh
// Copy business rules
java -jar neo-copy.jar -h eu3.hana.ondemand.com -t brm -sa <source_account> -ta <target_account> -soc_rr <rulesrepo_oauth_client_source> -socs_rr <rulesrepo_oauth_client_secret_source> -toc_rr <rulesrepo_oauth_client_target> -tocs_rr <rulesrepo_oauth_client_secret_target> -toc_rruntime <rulesruntime_oauth_client_target> -tocs_rruntime <rulesruntime_oauth_client_secret_target>
```
