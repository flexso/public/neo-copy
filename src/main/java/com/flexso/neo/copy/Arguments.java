package com.flexso.neo.copy;

import com.flexso.neo.copy.exceptions.CopyToolException;

import lombok.Data;

@Data
public class Arguments {

	private String host;
	private String sourceAccount;
	private String targetAccount;

	private TaskType type;

	// source
	OAuthClient oAuthClientSourceWorkflowRuntime;
	OAuthClient oAuthClientSourceBrmRepository;
	// target
	OAuthClient oAuthClientTargetWorkflowRuntime;
	OAuthClient oAuthClientTargetBrmRuntime;
	OAuthClient oAuthClientTargetBrmRepository;

	private String filter;
	private boolean testMode;

	public void checkArguments() throws CopyToolException {
		// source en target moeten ingevuld zijn
		if (host == null) {
			throw new CopyToolException("No host found in the arguments.");
		}

		if (sourceAccount == null) {
			throw new CopyToolException("No sourceAccount found in the arguments.");
		}

		if (targetAccount == null) {
			throw new CopyToolException("No targetAccount found in the arguments.");
		}

		if (type != TaskType.BRM && type != TaskType.WORKFLOW) {
			throw new CopyToolException("Only BRM and workflows are supported at the moment.");
		}

		switch (type) {
		case WORKFLOW:
			if (oAuthClientSourceWorkflowRuntime == null) {
				throw new CopyToolException("Missing oauth client for copying the workflows from the source account.");
			}
			if (oAuthClientTargetWorkflowRuntime == null) {
				throw new CopyToolException("Missing oauth client for copying the workflows to the target account.");
			}
			break;

		case BRM:
			if (oAuthClientSourceBrmRepository == null) {
				throw new CopyToolException("Missing oauth client for copying the rules from the source account.");
			}
			if (oAuthClientTargetBrmRepository == null) {
				throw new CopyToolException("Missing oauth client for copying the rules to the target account.");
			}
			if (oAuthClientTargetBrmRuntime == null) {
				throw new CopyToolException("Missing oauth client for deploying the rules to the target account.");
			}

			break;
		}

	}

}
