package com.flexso.neo.copy;

import lombok.Data;

@Data
public abstract class CopyTask implements ICopyTask {

	protected Arguments arguments;

	protected CopyTask(Arguments arg) {
		this.arguments = arg;
	}

}
