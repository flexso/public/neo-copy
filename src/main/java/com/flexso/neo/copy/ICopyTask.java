package com.flexso.neo.copy;

import com.flexso.neo.copy.exceptions.CopyToolException;

public interface ICopyTask {
	public void run() throws CopyToolException;
}
