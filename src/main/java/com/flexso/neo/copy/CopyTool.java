package com.flexso.neo.copy;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.flexso.neo.copy.exceptions.CopyToolException;
import com.flexso.neo.copy.rules.RulesRunner;
import com.flexso.neo.copy.workflow.WorkflowRunner;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class CopyTool {

	private Arguments arguments;

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		log.info("--- Starting copy tool ---");
		new CopyTool(args);
		log.info("--- Finished copy tool ---");

	}

	public CopyTool(String[] args) throws CopyToolException {
		Options options = new Options();

		options.addOption("sa", "source-account", true, "Id of the source account");
		options.addOption("ta", "target-account", true, "Id of the target account");
		options.addOption("h", "host", true, "Host - default eu.hana.ondemand.com");

		options.addOption("t", "type", true, "Type of object to copy [BRM or WORKFLOW]");
		options.addOption("test", "test", false, "If this flag is set, we only log the output and skip the requests that save something in the target.");

		options.addOption("soc_rr", "oauth-client-rules-runtime", true, "Source account Oauth client for rules-runtime");
		options.addOption("soc_wr", "oauth-client-workflow-runtime", true, "Source account Oauth client for workflow-runtime");
		options.addOption("toc_rr", "oauth-client-rules-runtime", true, "Target account Oauth client for rules-runtime");
		options.addOption("toc_wr", "oauth-client-workflow-runtime", true, "Target account Oauth client for workflow-runtime");
		options.addOption("toc_rruntime", "oauth-client-rules-repository", true, "Target account Oauth client for rules-repository");

		options.addOption("socs_rr", "oauth-client-rules-runtime-secret", true, "Secret for source account Oauth for rules-runtime");
		options.addOption("socs_wr", "oauth-client-workflow-runtime-secret", true, "Secret for source account Oauth for workflow-runtime");
		options.addOption("tocs_rr", "oauth-client-rules-runtime-secret", true, "Secret for target account Oauth for rules-runtime");
		options.addOption("tocs_wr", "oauth-client-workflow-runtime-secret", true, "Secret for target account Oauth for workflow-runtime");
		options.addOption("tocs_rruntime", "oauth-client-rules-repository-secret", true, "Secret for target account Oauth for rules-repository");

		options.addOption("f", "filter", true, "Filter on object names to copy");

		CommandLineParser parser = new DefaultParser();
		try {
			// parse the command line arguments
			CommandLine line = parser.parse(options, args);

			Option[] logoptions = line.getOptions();
			for (Option o : logoptions) {
				if (o.getLongOpt().contains("secret")) {
					log.info(o.getDescription() + ": " + o.getValue().replaceAll(".", "*"));
				} else {
					log.info(o.getDescription() + ": " + o.getValue());
				}
			}
			this.arguments = new Arguments();

			arguments.setHost(line.getOptionValue("h", "eu.hana.ondemand"));
			arguments.setSourceAccount(line.getOptionValue("sa"));
			arguments.setTargetAccount(line.getOptionValue("ta"));

			arguments.setOAuthClientSourceBrmRepository(new OAuthClient(line.getOptionValue("soc_rr"), line.getOptionValue("socs_rr")));
			arguments.setOAuthClientSourceWorkflowRuntime(new OAuthClient(line.getOptionValue("soc_wr"), line.getOptionValue("socs_wr")));
			arguments.setOAuthClientTargetBrmRepository(new OAuthClient(line.getOptionValue("toc_rr"), line.getOptionValue("tocs_rr")));
			arguments.setOAuthClientTargetWorkflowRuntime(new OAuthClient(line.getOptionValue("toc_wr"), line.getOptionValue("tocs_wr")));
			arguments.setOAuthClientTargetBrmRuntime(new OAuthClient(line.getOptionValue("toc_rruntime"), line.getOptionValue("tocs_rruntime")));

			arguments.setFilter(line.getOptionValue("filter"));
			arguments.setType(TaskType.valueOf(line.getOptionValue("t").toUpperCase()));

			arguments.setTestMode(line.hasOption("test"));
			arguments.checkArguments();

			log.info("Check which task to run ...");
			CopyTask copyTask = null;

			switch (arguments.getType()) {
			case WORKFLOW:
				copyTask = new WorkflowRunner(arguments);
				break;
			case BRM:
				copyTask = new RulesRunner(arguments);
				break;
			}

			if (copyTask != null) {
				copyTask.run();
			}

		} catch (ParseException e) {
			// oops, something went wrong
			log.error("Parsing failed.", e);
			throw new CopyToolException("Parsing failed", e);
		}

	}

}
