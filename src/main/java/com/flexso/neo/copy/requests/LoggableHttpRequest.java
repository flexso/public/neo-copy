package com.flexso.neo.copy.requests;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.http.client.utils.URIBuilder;
import org.stringtemplate.v4.ST;

import lombok.extern.log4j.Log4j2;

@Log4j2
public abstract class LoggableHttpRequest<V> implements Callable<V> {

	protected HttpURLConnection connection;
	protected String url;

	public LoggableHttpRequest(String urlStr, Map<String, String> parameters, HashMap<String, String> query) {
		ST urlTemplate = new ST(urlStr);

		for (String key : parameters.keySet()) {
			urlTemplate.add(key, parameters.get(key));
		}

		try {
			URIBuilder ub = new URIBuilder(urlTemplate.render());
			if (query != null) {
				for (String paramName : query.keySet()) {
					ub.addParameter(paramName, query.get(paramName));
				}
			}

			URL url = new URL(ub.build().toString());
			connection = (HttpURLConnection) url.openConnection();
			log.debug("Try to connect to " + url.toExternalForm());
		} catch (URISyntaxException | IOException e) {
			log.error("Cannot construct URL", e);
		}
	}

	@Override
	public V call() {
		log.debug("Starting " + this.getName() + " || " + connection.getURL().toExternalForm());
		try {
			return this.doRequest();
		} catch (IOException e) {
			log.error("Error in request " + this.getName(), e);
		}
		return null;
	}

	protected abstract V doRequest() throws IOException;

	protected abstract String getName();

}
