package com.flexso.neo.copy;

import com.google.common.io.BaseEncoding;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class OAuthClient {

	private String oAuthClientId;
	private String oAuthClientSecret;

	public String getAuthorizationValue() {
		String userpwd = new StringBuilder(this.getOAuthClientId()).append(":").append(this.getOAuthClientSecret()).toString();
		return new StringBuilder("Basic ").append(BaseEncoding.base64().encode(userpwd.getBytes())).toString();
	}

}
