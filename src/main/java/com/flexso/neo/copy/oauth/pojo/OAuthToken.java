package com.flexso.neo.copy.oauth.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class OAuthToken {

	private String access_token;
	private String token_type;
	private String expires_in;
	private String scope;

	public String getAuthorizationValue() {
		return "Bearer " + this.access_token;
	}

}
