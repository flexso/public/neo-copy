package com.flexso.neo.copy.oauth.requests;

import java.io.IOException;
import java.io.InputStreamReader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flexso.neo.copy.OAuthClient;
import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.requests.LoggableHttpRequest;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GetOauthToken extends LoggableHttpRequest<OAuthToken> {

	private OAuthClient client;
	private String account;

	public GetOauthToken(String account, String host, OAuthClient client) {
		super(URLS.OAUTH_URL, ImmutableMap.of("account", account, "host", host), null);

		this.client = client;
		this.account = account;
	}

	@Override
	protected OAuthToken doRequest() throws IOException {

		// setting request method
		connection.setRequestMethod("POST");

		// adding headers
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		// Basic Auth : provide username:password in Base64 encoded in Authorization
		// header
		connection.setRequestProperty("Authorization", client.getAuthorizationValue());

		connection.setDoInput(true);
		connection.setDoOutput(true);

		int responseCode = connection.getResponseCode();
		if (200 <= responseCode && responseCode <= 299) {
			String result = CharStreams.toString(new InputStreamReader(connection.getInputStream(), Charsets.UTF_8));
			ObjectMapper m = new ObjectMapper();
			OAuthToken token = m.readValue(result, OAuthToken.class);
			log.info("Token found for " + client.getOAuthClientId() + " on account " + account);
			return token;
		} else {
			String errorMessage = CharStreams.toString(new InputStreamReader(connection.getErrorStream(), Charsets.UTF_8));
			log.error("ERROR RESPONSE " + responseCode + ": " + errorMessage);
		}

		return null;
	}

	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return "Get oauthToken";
	}

}
