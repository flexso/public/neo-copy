package com.flexso.neo.copy.rules.requests;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.requests.LoggableHttpRequest;
import com.flexso.neo.copy.rules.pojo.DeployBody;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;

import lombok.extern.log4j.Log4j2;

@Deprecated
@Log4j2
public class PostDeployRuleService extends LoggableHttpRequest<Boolean> {

	private OAuthToken token;
	private DeployBody content;

	public PostDeployRuleService(String account, String host, OAuthToken token, DeployBody content) {
		super(URLS.getRuntimeURL(URLS.RULE_DEPLOY), ImmutableMap.of("account", account, "host", host), null);

		this.token = token;
		this.content = content;
	}

	@Override
	protected Boolean doRequest() throws IOException {

		// setting request method
		connection.setRequestMethod("POST");

		// adding headers
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		// Basic Auth : provide username:password in Base64 encoded in Authorization
		// header
		connection.setRequestProperty("Authorization", token.getAuthorizationValue());

		connection.setDoInput(true);
		connection.setDoOutput(true);

		// convert object to json
		ObjectMapper Obj = new ObjectMapper();
		String jsonStr = Obj.writeValueAsString(content);

		OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
		wr.write(jsonStr);
		wr.flush();

		log.debug("Try to deploy rule services: " + jsonStr);

		int responseCode = connection.getResponseCode();
		if (200 <= responseCode && responseCode <= 299) {
			// String result = CharStreams.toString(new
			// InputStreamReader(connection.getInputStream(), Charsets.UTF_8));
			log.info("Rule " + content.getRuleServiceName() + " of project " + content.getProjectName()
					+ "successfully deployed. " + responseCode);
			return Boolean.TRUE;

		} else {
			String errorMessage = CharStreams
					.toString(new InputStreamReader(connection.getErrorStream(), Charsets.UTF_8));
			log.error("ERROR RESPONSE " + responseCode + ": " + errorMessage);
		}

		return Boolean.FALSE;
	}

	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return "Deploy rule.";
	}

}
