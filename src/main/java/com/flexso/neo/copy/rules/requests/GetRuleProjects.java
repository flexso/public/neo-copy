package com.flexso.neo.copy.rules.requests;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.requests.LoggableHttpRequest;
import com.flexso.neo.copy.rules.pojo.Project;
import com.google.common.base.Charsets;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GetRuleProjects extends LoggableHttpRequest<HashMap<String, Project>> {

	private OAuthToken token;
	private String filter;

	public GetRuleProjects(String account, String host, OAuthToken token, String filter) {
		super(URLS.getURL(URLS.RULE_PROJECTS), ImmutableMap.of("account", account, "host", host), null);

		this.token = token;
		this.filter = filter;

	}

	@Override
	protected HashMap<String, Project> doRequest() throws IOException {

		// setting request method
		connection.setRequestMethod("GET");

		// adding headers
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		// Basic Auth : provide username:password in Base64 encoded in Authorization
		// header
		connection.setRequestProperty("Authorization", token.getAuthorizationValue());

		connection.setDoInput(true);
		connection.setDoOutput(true);

		int responseCode = connection.getResponseCode();
		if (200 <= responseCode && responseCode <= 299) {
			String result = CharStreams.toString(new InputStreamReader(connection.getInputStream(), Charsets.UTF_8));

			ObjectMapper m = new ObjectMapper();

			ArrayList<Project> output = m.readValue(result, new TypeReference<ArrayList<Project>>() {
			});

			if (filter != null) {
				Predicate<Project> predicate = new Predicate<Project>() {
					@Override
					public boolean apply(Project input) {
						return input.getName().equalsIgnoreCase(filter);
					}
				};

				output = new ArrayList<Project>(Collections2.filter(output, predicate));
			}

			HashMap<String, Project> resultMap = new HashMap<>();

			for (Project p : output) {
				if (resultMap.containsKey(p.getId())) {
					Project p2 = resultMap.get(p.getId());
					if (p.getVersion().getChangedOn().after(p2.getVersion().getChangedOn())) {
						resultMap.put(p.getId(), p);
					}
				} else {
					resultMap.put(p.getId(), p);
				}
			}
			return resultMap;
		} else {
			String errorMessage = CharStreams.toString(new InputStreamReader(connection.getErrorStream(), Charsets.UTF_8));
			log.error("ERROR RESPONSE " + responseCode + ": " + errorMessage);
		}

		return null;
	}

	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return "Retrieves the business rule projects.";
	}

}
