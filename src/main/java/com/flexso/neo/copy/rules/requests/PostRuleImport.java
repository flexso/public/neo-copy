package com.flexso.neo.copy.rules.requests;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.requests.LoggableHttpRequest;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class PostRuleImport extends LoggableHttpRequest<Boolean> {

	private OAuthToken token;
	private String content;

	public PostRuleImport(String account, String host, OAuthToken token, String content) {
		super(URLS.getURL(URLS.RULE_IMPORT), ImmutableMap.of("account", account, "host", host), null);

		this.token = token;
		this.content = content;
	}

	@Override
	protected Boolean doRequest() throws IOException {

		// setting request method
		connection.setRequestMethod("POST");

		// adding headers
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		// Basic Auth : provide username:password in Base64 encoded in Authorization
		// header
		connection.setRequestProperty("Authorization", token.getAuthorizationValue());

		connection.setDoInput(true);
		connection.setDoOutput(true);

		OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
		wr.write(content);
		wr.flush();

		int responseCode = connection.getResponseCode();
		if (200 <= responseCode && responseCode <= 299) {
			String result = CharStreams.toString(new InputStreamReader(connection.getInputStream(), Charsets.UTF_8));
			log.debug("Rules imported: " + result);
			return Boolean.TRUE;

		} else {
			String errorMessage = CharStreams.toString(new InputStreamReader(connection.getErrorStream(), Charsets.UTF_8));
			log.error("ERROR RESPONSE " + responseCode + ": " + errorMessage);
		}

		return Boolean.FALSE;
	}

	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return "Imports project to design time repository.";
	}

}
