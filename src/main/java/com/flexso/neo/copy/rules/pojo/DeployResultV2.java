package com.flexso.neo.copy.rules.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class DeployResultV2 {

	private String RuleServiceId;
	private String RuleServiceName;
	private String ProjectName;
	private String CreatedBy;
	private String CreatedOn;
	private String ChangedBy;
	private String ChangedOn;

}
