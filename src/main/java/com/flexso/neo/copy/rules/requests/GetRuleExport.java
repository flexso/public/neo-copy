package com.flexso.neo.copy.rules.requests;

import java.io.IOException;
import java.io.InputStreamReader;

import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.requests.LoggableHttpRequest;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GetRuleExport extends LoggableHttpRequest<String> {

	private OAuthToken token;

	public GetRuleExport(String account, String host, OAuthToken token, String id) {
		super(URLS.getURL(URLS.RULE_EXPORT), ImmutableMap.of("account", account, "host", host, "id", id), null);

		this.token = token;

	}

	@Override
	protected String doRequest() throws IOException {

		// setting request method
		connection.setRequestMethod("GET");

		// adding headers
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		// Basic Auth : provide username:password in Base64 encoded in Authorization
		// header
		connection.setRequestProperty("Authorization", token.getAuthorizationValue());

		connection.setDoInput(true);
		connection.setDoOutput(true);

		int responseCode = connection.getResponseCode();
		if (200 <= responseCode && responseCode <= 299) {
			String result = CharStreams.toString(new InputStreamReader(connection.getInputStream(), Charsets.UTF_8));
			log.debug("Export succeed: " + result);
			return result;

		} else {
			String errorMessage = CharStreams.toString(new InputStreamReader(connection.getErrorStream(), Charsets.UTF_8));
			log.error("ERROR RESPONSE " + responseCode + ": " + errorMessage);
		}

		return null;
	}

	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return "Retrieves the business rule projects.";
	}

}
