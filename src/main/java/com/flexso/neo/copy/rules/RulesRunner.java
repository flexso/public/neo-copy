package com.flexso.neo.copy.rules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.flexso.neo.copy.Arguments;
import com.flexso.neo.copy.CopyTask;
import com.flexso.neo.copy.exceptions.CopyToolException;
import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.oauth.requests.GetOauthToken;
import com.flexso.neo.copy.rules.pojo.DeployBodyV2;
import com.flexso.neo.copy.rules.pojo.DeployResultV2;
import com.flexso.neo.copy.rules.pojo.Project;
import com.flexso.neo.copy.rules.pojo.RuleService;
import com.flexso.neo.copy.rules.requests.GetRuleExport;
import com.flexso.neo.copy.rules.requests.GetRuleProjects;
import com.flexso.neo.copy.rules.requests.GetRuleServices;
import com.flexso.neo.copy.rules.requests.PostDeployRuleServiceV2;
import com.flexso.neo.copy.rules.requests.PostRuleImport;
import com.flexso.neo.copy.rules.requests.PutRuleActivation;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class RulesRunner extends CopyTask {

	public RulesRunner(Arguments arg) {
		super(arg);
	}

	@Override
	public void run() throws CopyToolException {

		ExecutorService executor = Executors.newCachedThreadPool();
		CopyToolException exceptions = new CopyToolException("Error occured when trying to transport rules");

		log.info("--- Start copy business rules ---");

		log.info("Get oAuth tokens for source & target");
		OAuthToken sourceToken = null;
		OAuthToken targetToken = null;
		OAuthToken targetRuntimeToken = null;
		try {
			sourceToken = executor.submit(new GetOauthToken(arguments.getSourceAccount(), arguments.getHost(),
					arguments.getOAuthClientSourceBrmRepository())).get();
			targetToken = executor.submit(new GetOauthToken(arguments.getTargetAccount(), arguments.getHost(),
					arguments.getOAuthClientTargetBrmRepository())).get();
			targetRuntimeToken = executor.submit(new GetOauthToken(arguments.getTargetAccount(), arguments.getHost(),
					arguments.getOAuthClientTargetBrmRuntime())).get();

		} catch (InterruptedException | ExecutionException e) {
			log.error("Unable to get oauth tokens for the source and target system.", e);
		}

		HashMap<String, Project> sourceProjects = new HashMap<>();
		HashMap<String, Project> targetProjects = new HashMap<>();

		try {
			sourceProjects = executor.submit(new GetRuleProjects(arguments.getSourceAccount(), arguments.getHost(),
					sourceToken, arguments.getFilter())).get();
		} catch (InterruptedException | ExecutionException e) {
			exceptions.add(e);
		}
		try {
			targetProjects = executor.submit(new GetRuleProjects(arguments.getTargetAccount(), arguments.getHost(),
					targetToken, arguments.getFilter())).get();
		} catch (InterruptedException | ExecutionException e) {
			exceptions.add(e);
		}

		HashMap<Project, Future<String>> jsonToImport = new HashMap<>();
		// loop over source projects and check if we need to read the export file ...
		for (String projectId : sourceProjects.keySet()) {
			Project sourceProject = sourceProjects.get(projectId);
			if (arguments.getFilter() != null) {
				log.info("Deploy project: " + sourceProject.getName());
				jsonToImport.put(sourceProject, executor.submit(new GetRuleExport(arguments.getSourceAccount(),
						arguments.getHost(), sourceToken, sourceProject.getId())));
			} else {
				if (targetProjects.containsKey(projectId)) {
					// check the version date on the target system
					Project targetProject = targetProjects.get(projectId);
					if (targetProject.getVersion().getChangedOn().before(sourceProject.getVersion().getChangedOn())) {
						log.info("Deploy project: " + sourceProject.getName());
						jsonToImport.put(sourceProject, executor.submit(new GetRuleExport(arguments.getSourceAccount(),
								arguments.getHost(), sourceToken, sourceProject.getId())));
					}
				} else {
					// deploy this verion of the rules
					log.info("Deploy project: " + sourceProject.getName());
					jsonToImport.put(sourceProject, executor.submit(new GetRuleExport(arguments.getSourceAccount(),
							arguments.getHost(), sourceToken, sourceProject.getId())));
				}
			}
		}

		if (!arguments.isTestMode()) {
			// deploy the rules to the target ...
			HashMap<Project, Future<Boolean>> imported = new HashMap<>();
			for (Project p : jsonToImport.keySet()) {
				try {
					imported.put(p, executor.submit(new PostRuleImport(arguments.getTargetAccount(),
							arguments.getHost(), targetToken, jsonToImport.get(p).get())));
				} catch (InterruptedException | ExecutionException e) {
					exceptions.add(e);
				}
			}

			HashMap<Project, Future<Boolean>> activated = new HashMap<>();
			for (Project p : imported.keySet()) {

				try {
					if (imported.get(p).get() == true) {
						activated.put(p, executor.submit(new PutRuleActivation(arguments.getTargetAccount(),
								arguments.getHost(), targetToken, p.getId())));
					}
				} catch (InterruptedException | ExecutionException e) {
					exceptions.add(e);
				}
			}

			// deploy the activated rules
			HashMap<Project, Future<ArrayList<RuleService>>> ruleservices = new HashMap<>();
			// HashMap<String, Future<Boolean>> deployed = new HashMap<>();
			HashMap<String, Future<DeployResultV2>> deployed = new HashMap<>();

			for (Project p : activated.keySet()) {
				try {
					if (activated.get(p).get() == true) {
						ruleservices.put(p, executor.submit(new GetRuleServices(arguments.getTargetAccount(),
								arguments.getHost(), targetToken, p.getId())));
					} else {
						log.error("Cannot activate rule: " + p.getName() + " :: " + p.getId());
						exceptions.add(
								new CopyToolException("Cannot activate rule: " + p.getName() + " :: " + p.getId()));
					}
				} catch (InterruptedException | ExecutionException e) {
					exceptions.add(e);
				}
			}

			for (Project p : ruleservices.keySet()) {
				try {
					ArrayList<RuleService> rules = ruleservices.get(p).get();
					for (RuleService rs : rules) {
						// DeployBody deployBody = new DeployBody(p.getName(), rs.getName());
						// deployed.put(p.getName() + " " + rs.getName(), executor.submit(new
						// PostDeployRuleService(arguments.getTargetAccount(), arguments.getHost(),
						// targetRuntimeToken, deployBody)));
						DeployBodyV2 deployBody = new DeployBodyV2(rs.getId());
						deployed.put(p.getName() + " " + rs.getName(),
								executor.submit(new PostDeployRuleServiceV2(arguments.getTargetAccount(),
										arguments.getHost(), targetRuntimeToken, deployBody)));
					}
				} catch (InterruptedException | ExecutionException e) {
					exceptions.add(e);
				}
			}

			for (String key : deployed.keySet()) {

				try {
					DeployResultV2 deployResult = deployed.get(key).get();
					// if (deployed.get(key).get() == true) {
					if (deployResult != null) {
						log.info("Rule deployed: " + deployResult.getProjectName() + " "
								+ deployResult.getRuleServiceName());
					} else {
						log.error("Cannot deploy rules service: " + key);
						exceptions.add(new CopyToolException("Cannot deploy rules service: " + key));
					}
				} catch (InterruptedException | ExecutionException e) {
					exceptions.add(e);
				}
			}
		}

		// wait for all promises
		try {
			executor.shutdown();
			executor.awaitTermination(10, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			exceptions.add(e);
		}

		if (exceptions.hasChildExceptions()) {
			throw exceptions;
		} else {
			if (arguments.isTestMode()) {
				log.warn("Import, activation & deployment is skipped in test mode. No errors occured.");
			} else {
				log.info("Rules successfully copied from " + arguments.getSourceAccount() + " to "
						+ arguments.getTargetAccount());
			}
		}

	}

}
