package com.flexso.neo.copy.rules.requests;

public class URLS {

	public static final String BASE_URL = "https://bpmrulesrepositoryrules-<account>.<host>/rules-service/rest/v1";
	public static final String BASE_RUNTIME_URL = "https://bpmrulesruntimerules-<account>.<host>/rules-service";

	public static final String RULE_PROJECTS = "/projects";
	public static final String RULE_EXPORT = "/export/projects/<id>";
	public static final String RULE_IMPORT = "/import/projects";
	public static final String RULE_ACTIVATION = "/projects/<id>/activation";
	public static final String RULE_SERVICES = "/projects/<id>/ruleservices";

	public static final String RULE_DEPLOY = "/rest/v1/rule-definitions/java";
	public static final String RULE_DEPLOY_V2 = "/rest/v2/workingset-rule-definitions";

	public static String getURL(String path) {
		return new StringBuilder(BASE_URL).append(path).toString();
	}

	public static String getRuntimeURL(String path) {
		return new StringBuilder(BASE_RUNTIME_URL).append(path).toString();
	}

}
