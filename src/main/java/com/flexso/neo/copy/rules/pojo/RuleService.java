package com.flexso.neo.copy.rules.pojo;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
public class RuleService {
	private String Id;
	private String Project;
	private String Name;
	private ArrayList<TranslatedText> Description;
	private ArrayList<TranslatedText> Label;
}
