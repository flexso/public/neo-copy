package com.flexso.neo.copy.workflow.pojo;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class WorkflowDefinition {
	private String id;
	private String name;
	private String version;
	private String createdBy;
	private Date createdAt;
}
