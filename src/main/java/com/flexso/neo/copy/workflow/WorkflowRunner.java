package com.flexso.neo.copy.workflow;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.flexso.neo.copy.Arguments;
import com.flexso.neo.copy.CopyTask;
import com.flexso.neo.copy.exceptions.CopyToolException;
import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.oauth.requests.GetOauthToken;
import com.flexso.neo.copy.workflow.pojo.WorkflowDefinition;
import com.flexso.neo.copy.workflow.requests.GetWorkflowDefinitions;
import com.flexso.neo.copy.workflow.requests.GetWorkflowModel;
import com.flexso.neo.copy.workflow.requests.PostWorkflowModel;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class WorkflowRunner extends CopyTask {

	public WorkflowRunner(Arguments arg) {
		super(arg);
	}

	@Override
	public void run() throws CopyToolException {

		ExecutorService executor = Executors.newCachedThreadPool();
		CopyToolException exceptions = new CopyToolException("Error occured when trying to transport rules");

		log.info("--- Start copy workflow ---");

		log.info("Get oAuth tokens for source & target");
		OAuthToken sourceToken = null;
		OAuthToken targetToken = null;
		try {
			sourceToken = executor.submit(new GetOauthToken(arguments.getSourceAccount(), arguments.getHost(), arguments.getOAuthClientSourceWorkflowRuntime())).get();
			targetToken = executor.submit(new GetOauthToken(arguments.getTargetAccount(), arguments.getHost(), arguments.getOAuthClientTargetWorkflowRuntime())).get();

		} catch (InterruptedException | ExecutionException e) {
			log.error("Unable to get oauth tokens for the source and target system.", e);
		}

		// get the workflow definitions of the source system
		ArrayList<WorkflowDefinition> sourceDefList = new ArrayList<>();
		try {
			sourceDefList = executor.submit(new GetWorkflowDefinitions(arguments.getSourceAccount(), arguments.getHost(), sourceToken, arguments.getFilter())).get();
		} catch (InterruptedException | ExecutionException e) {
			exceptions.add(e);
		}
		ArrayList<WorkflowDefinition> targetDefList = new ArrayList<>();
		try {
			targetDefList = executor.submit(new GetWorkflowDefinitions(arguments.getTargetAccount(), arguments.getHost(), targetToken, arguments.getFilter())).get();
		} catch (InterruptedException | ExecutionException e) {
			exceptions.add(e);
		}

		ImmutableMap<String, WorkflowDefinition> targetDefMap = Maps.uniqueIndex(targetDefList, WorkflowDefinition::getId);

		HashMap<WorkflowDefinition, Future<String>> modelsToDeploy = new HashMap<>();

		for (WorkflowDefinition sourceDef : sourceDefList) {
			if (arguments.getFilter() != null) {
				modelsToDeploy.put(sourceDef, executor.submit(new GetWorkflowModel(arguments.getSourceAccount(), arguments.getHost(), sourceToken, sourceDef.getId())));
				log.info("Workflow to transport: " + sourceDef.getName());
			} else {
				if (targetDefMap.containsKey(sourceDef.getId())) {
					WorkflowDefinition targetDef = targetDefMap.get(sourceDef.getId());

					if (targetDef.getCreatedAt().before(sourceDef.getCreatedAt())) {
						modelsToDeploy.put(sourceDef, executor.submit(new GetWorkflowModel(arguments.getSourceAccount(), arguments.getHost(), sourceToken, sourceDef.getId())));
						log.info("Workflow to transport: " + sourceDef.getName());
					}
				} else {
					modelsToDeploy.put(sourceDef, executor.submit(new GetWorkflowModel(arguments.getSourceAccount(), arguments.getHost(), sourceToken, sourceDef.getId())));
					log.info("Workflow to transport: " + sourceDef.getName());
				}
			}
		}

		for (WorkflowDefinition wf : modelsToDeploy.keySet()) {
			// for (Future<String> wfToDeploy : modelsToDeploy.values()) {
			try {
				String model = modelsToDeploy.get(wf).get();
				if (!arguments.isTestMode()) {
					log.debug("deploy: " + model);
					executor.submit(new PostWorkflowModel(arguments.getTargetAccount(), arguments.getHost(), targetToken, model, wf.getName()));
				}
			} catch (InterruptedException | ExecutionException e) {
				exceptions.add(e);
			}
		}

		// wait for all promises
		try {
			executor.shutdown();
			executor.awaitTermination(10, TimeUnit.MINUTES);
		} catch (InterruptedException e) {
			exceptions.add(e);
		}

		if (exceptions.hasChildExceptions()) {
			throw exceptions;
		} else {
			if (arguments.isTestMode()) {
				log.warn("Deployment is skipped in test mode. No errors occured.");
			} else {
				log.info("Workflows successfully copied from " + arguments.getSourceAccount() + " to " + arguments.getTargetAccount());
			}
		}

	}

}
