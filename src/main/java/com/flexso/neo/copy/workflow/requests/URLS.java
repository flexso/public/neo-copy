package com.flexso.neo.copy.workflow.requests;

public class URLS {

	public static final String BASE_URL = "https://bpmworkflowruntimeworkflow-<account>.<host>/workflow-service/rest";

	public static final String WF_DEFINITIONS = "/v1/workflow-definitions";
	public static final String WF_MODELS = "/v1/workflow-definitions/<id>/model";
	public static final String WF_DEPLOY = "/internal/v1/workflow-definitions";

	public static String getURL(String path) {
		return new StringBuilder(BASE_URL).append(path).toString();
	}

}
