package com.flexso.neo.copy.workflow.requests;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.requests.LoggableHttpRequest;
import com.flexso.neo.copy.workflow.pojo.WorkflowDefinition;
import com.google.common.base.Charsets;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GetWorkflowDefinitions extends LoggableHttpRequest<ArrayList<WorkflowDefinition>> {

	private OAuthToken token;
	private String filter;

	public GetWorkflowDefinitions(String account, String host, OAuthToken token, String filter) {
		super(URLS.getURL(URLS.WF_DEFINITIONS), ImmutableMap.of("account", account, "host", host), null);

		this.token = token;
		this.filter = filter;

	}

	@Override
	protected ArrayList<WorkflowDefinition> doRequest() throws IOException {

		// setting request method
		connection.setRequestMethod("GET");

		// adding headers
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		// Basic Auth : provide username:password in Base64 encoded in Authorization
		// header
		connection.setRequestProperty("Authorization", token.getAuthorizationValue());

		connection.setDoInput(true);
		connection.setDoOutput(true);

		int responseCode = connection.getResponseCode();
		if (200 <= responseCode && responseCode <= 299) {
			String result = CharStreams.toString(new InputStreamReader(connection.getInputStream(), Charsets.UTF_8));
			ObjectMapper m = new ObjectMapper();

			ArrayList<WorkflowDefinition> output = m.readValue(result, new TypeReference<ArrayList<WorkflowDefinition>>() {
			});

			if (filter != null) {
				Predicate<WorkflowDefinition> predicate = new Predicate<WorkflowDefinition>() {
					@Override
					public boolean apply(WorkflowDefinition input) {
						return input.getName().equalsIgnoreCase(filter);
					}
				};

				output = new ArrayList<WorkflowDefinition>(Collections2.filter(output, predicate));
			}

			return output;
		} else {
			String errorMessage = CharStreams.toString(new InputStreamReader(connection.getErrorStream(), Charsets.UTF_8));
			log.error("ERROR RESPONSE " + responseCode + ": " + errorMessage);
		}

		return null;
	}

	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return "Get workflow definitions";
	}

}
