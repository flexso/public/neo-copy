package com.flexso.neo.copy.workflow.requests;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.flexso.neo.copy.oauth.pojo.OAuthToken;
import com.flexso.neo.copy.requests.LoggableHttpRequest;
import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class PostWorkflowModel extends LoggableHttpRequest<String> {

	private OAuthToken token;
	private String model;
	private String wfname;

	public PostWorkflowModel(String account, String host, OAuthToken token, String model, String wfname) {
		super(URLS.getURL(URLS.WF_DEPLOY), ImmutableMap.of("account", account, "host", host), null);

		this.token = token;
		this.model = model;
		this.wfname = wfname;
	}

	@Override
	protected String doRequest() throws IOException {

		// setting request method
		connection.setRequestMethod("POST");

		// adding headers
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Accept", "application/json");

		// Basic Auth : provide username:password in Base64 encoded in Authorization
		// header
		connection.setRequestProperty("Authorization", token.getAuthorizationValue());

		connection.setDoInput(true);
		connection.setDoOutput(true);

		OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
		wr.write(model);
		wr.flush();

		int responseCode = connection.getResponseCode();
		if (200 <= responseCode && responseCode <= 299) {
			String result = CharStreams.toString(new InputStreamReader(connection.getInputStream(), Charsets.UTF_8));
			log.info("Workflow " + wfname + " is deployed with response code: " + responseCode);
			return result;
		} else {
			String errorMessage = CharStreams.toString(new InputStreamReader(connection.getErrorStream(), Charsets.UTF_8));
			log.error("ERROR RESPONSE " + responseCode + ": " + errorMessage);
		}

		return null;
	}

	@Override
	protected String getName() {
		// TODO Auto-generated method stub
		return "Get Workflow Model";
	}

}
