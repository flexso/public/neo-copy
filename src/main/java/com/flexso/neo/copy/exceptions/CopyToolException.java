package com.flexso.neo.copy.exceptions;

public class CopyToolException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8964104432945256123L;

	public CopyToolException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CopyToolException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CopyToolException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public void add(Exception e) {
		this.addSuppressed(e);
	}

	public boolean hasChildExceptions() {
		return this.getSuppressed().length > 0;
		// return innerExeptions.size() > 0;
	}

}
