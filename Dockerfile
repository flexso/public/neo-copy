FROM gradle:5.4.1-jdk8-slim AS build
USER root
WORKDIR /gradle_build
COPY . /gradle_build
RUN gradle build && \
    cp ./build/distributions/neo-copy.zip /

FROM openjdk:8-jre-alpine as tool
USER root
COPY --from=build /neo-copy.zip /neo-copy.zip
RUN unzip -o /neo-copy.zip -d . && \
    rm /neo-copy.zip
WORKDIR /neo-copy/